package presentacion;

import logica.UsuariosDTO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RegistroUsuarios {
    private JTextField txtNombre;
    private JButton btnGuardar;
    private JPasswordField txtPassword;
    private JTextField txtEmail;
    private JPanel panelFormularioRegistro;
    private JLabel lblError;
    private JButton btnVerUsuarios;
    private JComboBox cbxSexo;
    private JButton irABuscadorDeButton;

    public RegistroUsuarios() {

        cbxSexo.addItem("Masculino");
        cbxSexo.addItem("Femenido");

        btnGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                lblError.setText("");

                System.out.println(cbxSexo.getSelectedItem());

                UsuariosDTO usuariosDTO = new UsuariosDTO(
                        txtNombre.getText(),
                        txtEmail.getText(),
                        txtPassword.getPassword()
                );

                if(valadarCamposVacios() || !soloNumeros()){
                    lblError.setText("Hay errores en el formulario!");
                }else {
                    if(usuariosDTO.procesarRegistro()){
                        JOptionPane.showMessageDialog(
                                panelFormularioRegistro,
                                "Usuario se guardo con exito",
                                "Estado de la Operacion",
                                JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(
                                panelFormularioRegistro,
                                "Error en el guardado",
                                "Estado de la Operacion",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }




            }
        });

        btnVerUsuarios.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame();
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                VerUsuarios verUsuarios = new VerUsuarios();
                frame.setContentPane(verUsuarios.getPanelverUsuarios());
                frame.setTitle("Usuarios Registrados");
                frame.setSize(600, 600);
                frame.setVisible(true);
            }
        });
        irABuscadorDeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame();
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                BuscadorPais buscadorPais = new BuscadorPais();
                frame.setContentPane(buscadorPais.getPanelBuscar());
                frame.setTitle("Buscador de paises");
                frame.setSize(600, 600);
                frame.setVisible(true);
            }
        });
    }

    public JPanel getPanelFormularioRegistro() {

        return this.panelFormularioRegistro;
    }

    public boolean valadarCamposVacios(){

        if(this.txtNombre.getText().isEmpty() ||
                this.txtEmail.getText().isEmpty() ||
                this.txtPassword.getPassword().equals("")) {
            return true;
        }
        return false;


    }

    public boolean soloNumeros(){
        if (this.txtNombre.getText().matches("[a-zA-Z]+")){
            return true;
        }
        return false;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}

package presentacion;

import logica.UsuariosDTO;

import javax.swing.*;

public class VerUsuarios {
    private JTable tableVerUsuarios;
    private JPanel panelVerUsuarios;

    public VerUsuarios(){
        UsuariosDTO usuariosDTO = new UsuariosDTO();
        this.tableVerUsuarios.setModel(usuariosDTO.cargarTabla());
    }


    public JPanel getPanelverUsuarios() {
        return this.panelVerUsuarios;
    }

}

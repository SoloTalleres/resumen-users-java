package presentacion;

import logica.ConsultaApiPaisesDTO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class BuscadorPais {
    private JTextField txtBuscar;
    private JButton btnBuscar;
    private JPanel panelBuscar;
    private JLabel lblNombrePais;
    private JLabel lblPoblacion;
    private JLabel lblGini;

    public BuscadorPais() {
    btnBuscar.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            ConsultaApiPaisesDTO consultaApiPaisesDTO = new ConsultaApiPaisesDTO();
            try {
                String resultado = consultaApiPaisesDTO.getPaisInfo(txtBuscar.getText());
                lblNombrePais.setText(resultado);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    });
}
public JPanel getPanelBuscar(){
    return this.panelBuscar;
}
}

package logica;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ConsultaApiPaisesDTO {

    private static final String BASE_URL = "https://restcountries.com/v3.1/name/";
    private static final String FIELDS = "?fields=name,capital,currencies,gini";

    public String getPaisInfo(String countryName) throws IOException {
        String formattedCountryName = countryName.replaceAll("\\s+", "%20");
        URL url = new URL(BASE_URL + formattedCountryName + FIELDS);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        int responseCode = connection.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + responseCode);
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode json = objectMapper.readTree(response.toString());

        String capital = json.get(0).get("capital").get(0).asText();
        double gini = json.get(0).get("gini").get("2019").asDouble();
        System.out.println(gini);
        System.out.println(capital);
        System.out.println(json.get(0).get("name").get("official"));


        return response.toString();
    }
}



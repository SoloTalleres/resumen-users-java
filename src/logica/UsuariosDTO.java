package logica;


import persistencia.UsuariosDAO;

import javax.swing.table.DefaultTableModel;
import java.sql.ResultSet;

public class UsuariosDTO {
    private String nombre;
    private String email;
    private char[] password;

    public UsuariosDTO(){}
    public UsuariosDTO(String nombre, String email, char[] password){
        this.nombre = nombre;
        this.email = email;
        this.password = password;
    }

    public boolean procesarRegistro(){

        UsuariosDAO usuariosDAO = new UsuariosDAO();
        boolean status = usuariosDAO.crear(
                this.nombre,
                this.email,
                String.valueOf(this.password)
        );

        if(status){
            return true;
        }
        return false;

    }

    public DefaultTableModel cargarTabla(){
        UsuariosDAO usuariosDAO = new UsuariosDAO();
        ResultSet data = usuariosDAO.listar();
        try{
            String[] nombreColumnas = {"id", "Nombre", "Email", "Password"};
            DefaultTableModel defaultTableModel = new DefaultTableModel(nombreColumnas, 1);
            while (data.next()){
                Object[] filas = {
                        data.getInt("id"),
                        data.getString("nombre"),
                        data.getString("email"),
                        data.getString("password")
                };
                defaultTableModel.addRow(filas);
            }

            return defaultTableModel;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

}

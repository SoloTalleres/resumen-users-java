package persistencia;

import java.sql.*;

public class UsuariosDAO {

    private Connection connection;

    public UsuariosDAO(){
        Conexion conexion = new Conexion();
        this.connection = conexion.getConnection();
        this.crearTabla();
    }

    private void crearTabla(){
        try{
            Statement statement = this.connection.createStatement();
            statement.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS usuarios (id INTEGER PRIMARY KEY, nombre TEXT, email TEXT, password TEXT ) ");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public boolean crear(String nombre, String email, String password){
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "INSERT INTO usuarios(nombre, email, password) VALUES (?, ?, ?) "
            );
            preparedStatement.setString(1, nombre);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, password);
            preparedStatement.execute();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }

    }

    public ResultSet listar(){
        ResultSet resultSet = null;
        try{
            Statement statement = this.connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM usuarios");
            return resultSet;

        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

}

import presentacion.RegistroUsuarios;

import javax.swing.JFrame;

public class Main {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        RegistroUsuarios registroUsuarios = new RegistroUsuarios();
        frame.setContentPane(registroUsuarios.getPanelFormularioRegistro());
        frame.setTitle("Formulario de registro");
        frame.setSize(600, 600);
        frame.setVisible(true);
    }
}